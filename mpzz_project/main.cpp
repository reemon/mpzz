
#include <iostream>
#include <ctime>
#include <fstream>
#include <sstream>

struct Stats {
	int swapCount;
	int recursionCount;
	int comparisonCount;
	bool qsort;
};

const int ARR_LEN = 1000000;
const char* INSERTION_SORT = "insort";
const char* QUICK_SORT = "qsort";
const char* GENERATE = "generate";
const char* CHECK_ORDER = "checkorder";

Stats stats;

using namespace std;

//init funkce
double startInsertionSort(char* datafile, char* outfile, int arrLen);
double startQuickSort(char* datafile, char* outfile, int arrLen);

//razeni
void quickSort(int* arr, int arrLen);
void insertionSort(int* arr, int arrLen);

//data
void generateData(int arrLen, char* filename);
void readData(int* arr, int arrLen, char* filename);
void writeData(int* arr, int arrLen, char* filename);

//utils
void print(int *arr, int arrLen) {
	for(int i = 0; i < arrLen; i++) {
		std::cout << arr[i] << std::endl;
	}
}

bool checkOrder(int* arr, int arrLen, bool asc = false) {
	for(int i = 0; i < arrLen - 1; i++) {
		if( (! asc && arr[i] < arr[i+1]) || (asc && arr[i] > arr[i+1])) {
			return false;
		}
	}
	return true;
}

int main(int argc, char** argv) {
	if(argc <= 1) {
		cerr << "Zadejte typ razeni " << QUICK_SORT << "/" << INSERTION_SORT << "!";
		cin.get();
		return -1;
	}

	if(strcmp(CHECK_ORDER, argv[1]) == 0) {
		int len = atoi(argv[2]);
		int order = atoi(argv[4]);
		int* arr = new int[len];	
		readData(arr, len, argv[3]);
		if(checkOrder(arr, len, order == 1 ? true : false)) {
			cout << "Order " << (order == 1 ? " asc: " : " desc: ") << "TRUE";
		} else {
			cout << "Order " << (order == 1 ? " asc: " : " desc: ") << "FALSE";
		}
		delete[] arr;
		return 0;
	}

	if(strcmp(GENERATE, argv[1]) == 0) {
		generateData(atoi(argv[2]), argv[3]);
		return 0;
	}

	int len = 1000000;
	if(argc >= 3) {
		if( ! isdigit(argv[2][0]) ) {
			len = 1000000;
		} else {
			len = atoi(argv[2]);
		}
	}

	char* type = argv[1];
	char* datafile = argc >= 4 ? argv[3] : "data.txt";
	char* outfile = new char[8+strlen(type)];
	
	if(argc >= 5) {
		outfile = argv[4];
	} else {
		sprintf(outfile, "out-%s.txt", type);	
	} 

	cout << "Razeni " << type << endl;

	stats.recursionCount = 0;
	stats.swapCount = 0;
	stats.comparisonCount = 0;

	double elapsedTime = 0.0;
	if(strcmp(type, INSERTION_SORT) == 0) {
		elapsedTime = startInsertionSort(datafile, outfile, len);
	}else if(strcmp(type, QUICK_SORT) == 0) {
		elapsedTime = startQuickSort(datafile, outfile, len);
	}else{
		cerr << "Nedefinovany typ razeni!";
		cin.get();
		return -1;
	}

	cout << "Doba behu " << type << " pro " << len << " prvku: " << elapsedTime << " sec" << endl;
	cout << "Vysledky ulozeny v " << outfile << endl;

	cin.get();

	return 0;
}

//
double startInsertionSort(char* datafile, char* outfile, int arrLen) {
	stats.qsort = false;

	clock_t begin = clock();
	int* arr = new int[arrLen];
	
	readData(arr, arrLen, datafile);

	clock_t bq = clock();
	insertionSort(arr, arrLen);
	cout << "Cista doba behu: " << double(clock() - bq) / CLOCKS_PER_SEC << endl;

	writeData(arr, arrLen, outfile);
	
	delete[] arr;

	return double(clock() - begin) / CLOCKS_PER_SEC;
}

double startQuickSort(char* datafile, char* outfile, int arrLen) {
	stats.qsort = true;

	clock_t begin = clock();
	int* arr = new int[arrLen];

	readData(arr, arrLen, datafile);

	clock_t bq = clock();
	quickSort(arr, arrLen);
	cout << "Cista doba behu: " << double(clock() - bq) / CLOCKS_PER_SEC << endl;

	writeData(arr, arrLen, outfile);

	delete[] arr;

	return double(clock() - begin) / CLOCKS_PER_SEC;
}

//READ/WRITE DATA
void generateData(int arrLen, char* filename){
	stringstream stream;
	int n = 0;
	for(int i = 0; i < arrLen; i++) {
		stream << (rand() * rand()) % arrLen << endl;
	}
	
	ofstream data(filename);
	data.write(stream.str().c_str(), stream.str().length());
	data.close();
}

void readData(int* arr, int arrLen, char* filename){
	ifstream data(filename);

	for(int i = 0; i < arrLen && data >> arr[i]; i++) {}

	data.close();
}


void writeData(int* arr, int arrLen, char* filename) {
	stringstream stream;
	if(stats.qsort) {
		stream << "Recursion count: " << stats.recursionCount << endl;
	}
	stream << "Swap count: " << stats.swapCount << endl;
	stream << "Comparison count: " << stats.comparisonCount << endl;

	double time = 0.0;
	int n = 0;
	
	for(int i = 0; i < arrLen; i++) {		
		stream << arr[i] << endl;		
	}

	string outstr = stream.str();
	ofstream data(filename);
	data.write(outstr.c_str(), outstr.length());
	data.close();
}


//SORTING ALG
void swap(int &a, int &b) {
	if(a == b) {
		return;
	}

	int t = a;
	a = b;
	b = t;
	stats.swapCount++;
}

void insertionSort(int* arr, int arrLen) {
	int rightV;
	int rightI;
	//#insfor
	for(int i = 0; i < arrLen - 1; i++) {
		//#insvar
		rightI = (i + 1);
		rightV = arr[rightI];

		//#inscompare
		if(rightV == arr[i] || rightV < arr[i]) {
			continue;
		}
		stats.comparisonCount += 2;
		
		//#inswhile
		while(rightI > 0 && rightV > arr[rightI - 1]) {
			stats.comparisonCount++;
			stats.swapCount++;

			//#insshift
			arr[rightI] = arr[rightI - 1];
			rightI--;
		}
		//#insset
		arr[rightI] = rightV;
	}
}

void quickSort(int* arr, int arrLen) {
	//#qsortArrLen
	if(arrLen <= 1) {
		return;
	}
	
	if(arrLen <= 10) {
		insertionSort(arr, arrLen);
		return;
	}

	stats.recursionCount++;

	//urceni pivotu
	int firstValue = arr[0];
	int midValue = arr[arrLen/2];
	int lastValue = arr[arrLen-1];

	//#qsortPivot
	int pivot = firstValue;
	if((midValue > firstValue && midValue < lastValue) || (midValue > lastValue && midValue < firstValue)){
		pivot = midValue;
	}else if((lastValue > firstValue && lastValue < midValue) || (lastValue > midValue && lastValue < firstValue)) {
		pivot = lastValue;
	}

	//rozdeleni pole do dvou polovin
	int left = -1; //kvuli do while, prvni se pricte a potom porovnava
	int right = arrLen;
	while(true) {
		do{
			left++;
			stats.comparisonCount++;
		}while (arr[left] > pivot);
		do{
			right--;
			stats.comparisonCount++;
		}while (arr[right] < pivot);

		if(left < right) {
			stats.comparisonCount++;
			swap(arr[left], arr[right]);
			continue;
		}
	
		break;
	}

	quickSort(arr, left);
	quickSort(arr+right+1, arrLen-(right+1));

}